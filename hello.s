	.intel_syntax noprefix

	.section .text
	.global _start
_start:	
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, OFFSET msg
	mov	rdx, OFFSET len
	syscall

	mov	rax, 60
	mov	rdi, 0
	syscall

msg:
	.ascii "Hello world!\n"

len = . - msg
