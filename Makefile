AS = as
CC = gcc
LDFLAGS = -nostdlib -nodefaultlibs -no-pie

name =  hello

.PHONY: all clean

all: $(name)

$(name): $(name).o
	ld -o $(name) $(name).o

clean:
	rm -f $(name) $(name).o
